# mind+离线语音识别及合成模块图形化库

#### 介绍
mind+离线语音识别及合成模块第三方图形化库，不限定命令词，支持所有人自定义的固件。同时支持识别和合成功能。支持固定和自定义格式。最多支持12个不同类型多参数播报。
#### 软件架构
mind+


#### 安装教程

1.利用https://gitee.com/LNSFAIoT/offline-speech-recognition-and-synthesis-modules复制在mind+用户库搜索栏中进行在线下载；

2.选择直接下载lnsfaiot-offline-speech-recognition-and-synthesis-modules-thirdex-V0.0.1.mpext文件，直接外部导入。

#### 使用说明
图形化积木截图

![输入图片说明](%E5%9B%BE%E5%BD%A2%E5%8C%96%E7%A7%AF%E6%9C%A8%E6%88%AA%E5%9B%BEimage.png)

测试源码截图

![输入图片说明](%E6%B5%8B%E8%AF%95%E6%BA%90%E7%A0%81%E6%88%AA%E5%9B%BEimage.png)
#### 参与贡献
岭师人工智能素养教育共同体

微信公众号：人工智能素养教育共同体

![输入图片说明](image.png)
